import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatInputModule, MatSidenavModule, MatFormFieldModule, MatSnackBarModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app-component/app.component';
import { LoginFormComponent } from './modules/login/components/login-form/login-form.component';
import { RouterModule, Routes } from '@angular/router';
import { AccessAuthGuard } from './shared/guards/accessAuth.guard';
import { DashboardComponent } from './modules/dashboard/components/dashboard/dashboard.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { PageLayoutComponent } from './layouts/page-layout/page-layout.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RegisterFormComponent } from './modules/register/components/register-form/register-form.component';
import { RadarChartComponent } from './modules/dashboard/components/radar-chart/radar-chart.component';
import { ImageCaptureComponent } from './shared/components/image-capture/image-capture.component';
import { NotificationsService } from './shared/services/notifications.service';
import { AccessAlreadyAuthGuard } from './shared/guards/accesAlreadyAuth.guard';
import { ChartEmotionComponent } from './modules/dashboard/components/chart-emotion/chart-emotion.component';

export const appRoutes: Routes = [
  {
    path: 'login',
    canActivate: [AccessAlreadyAuthGuard],
    component: LoginFormComponent
  },
  {
    path: 'register',
    canActivate: [AccessAlreadyAuthGuard],
    component: RegisterFormComponent
  },
  {
    path: '',
    component: PageLayoutComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AccessAuthGuard],
      }
    ]
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full' 
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    DashboardComponent,
    HeaderComponent,
    PageLayoutComponent,
    RegisterFormComponent,
    RadarChartComponent,
    ImageCaptureComponent,
    ChartEmotionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatSnackBarModule,
    FormsModule,
    ChartsModule,
    ReactiveFormsModule,
    CommonModule,
    AngularFontAwesomeModule,    
    RouterModule.forRoot(appRoutes, {enableTracing: false})
  ],
  providers: [NotificationsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
