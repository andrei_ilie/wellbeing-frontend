export class PaginationModel<T> {
    count: Number;
    next: string;
    prievios: string;
    results: Array<T>;
}