import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
  })
export class AccessAuthGuard implements CanActivate {

    constructor(private router: Router) {       
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let token = localStorage.getItem("token");
        if(token != null) {
            return true;
        }

        return this.router.parseUrl('/login');
    }
}