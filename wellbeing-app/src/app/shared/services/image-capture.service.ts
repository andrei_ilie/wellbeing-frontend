import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { ImageModel } from '../models/image.model';
import { ImageResponse } from '../models/image-response.model';

@Injectable({
  providedIn: 'root'
})
export class ImageCaptureService {
  private _requestUrl: string = environment.emotionsUrl + "send_photo";

  constructor(private _http: HttpClient) { 
  }

  async postImageCapture(imageModel: ImageModel): Promise<HttpResponse<ImageResponse>> {
    return this._http.post<ImageResponse>(this._requestUrl, imageModel, { headers: new HttpHeaders().set('token', localStorage.getItem('token')), observe: "response" }).toPromise();
  }
}
