import { Injectable, OnInit } from "@angular/core";

@Injectable()
export class NotificationsService implements OnInit {
    permission: string;

    ngOnInit(): void {
        if (!("Notification" in window)) {
            throw new Error("The browser does not support notifications");
        }
    }

    showNotification(message: string, body: string, icon:string = ''): void {      
        if (Notification.permission !== "granted") {
            Notification.requestPermission().then((permission) => {
                if (permission == "granted") {
                   this.deployNotification(message, body, icon);
                }
            });
        } else {
            this.deployNotification(message, body, icon);         
        }
    }

    private deployNotification(title, body, icon) {
        const notification = new Notification(title, {body, icon});
        notification.onclick = function () {
            if(body.substr(0,4) == "http" || body.substr(0,5) == "https") {
                window.open(body, "_blank");
            } else {
                window.open("https://www.google.com/search?q=10+things+to+do+to+be+more+happy", "_blank");
            }
        }
    }
}