import { Component } from '@angular/core';
import { NotificationsService } from 'app/shared/services/notifications.service';
import { ImageCaptureService } from 'app/shared/services/image-capture.service';
import { ImageModel } from 'app/shared/models/image.model';

@Component({
    selector: 'app-image-capture',
    templateUrl: './image-capture.component.html',
    styleUrls: ['./image-capture.component.scss']
})
export class ImageCaptureComponent {

    constructor(
            private imageCaptureService: ImageCaptureService) {
        this.captureImage();
    }

    captureImage() {
        const timeInterval = 15000;
    
        navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
            let video:any = document.getElementById('video');  
            video.srcObject = stream;
    
            setInterval(_ => {
                video.play();

                let canvas:any = document.getElementById("canvas");
                canvas.width = video.videoWidth;
                canvas.height = video.videoHeight;

                let context = canvas.getContext("2d");
                context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);

                let data = canvas.toDataURL();
                data = data.replace('data:image/png;base64,', '');
    
                video.pause();
                
                let model = new ImageModel();
                model.photo = data;
                model.user_token = '1'; // hard codded
                
                this.imageCaptureService.postImageCapture(model).then(result => {
                    if(result.status !== 200) {
                        console.log('Error uploading the photo');
                    }
                    console.log("succes send");
                },
                error => {
                    console.log('Error uploading the photo from api');
                    console.log(error);
                });          
            }, timeInterval);
        });
    }
}
