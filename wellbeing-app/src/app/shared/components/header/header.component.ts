import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private _router: Router) { }

  name: string;

  ngOnInit() {
    this.name = localStorage.getItem("name");
  }

  public logout() {
    localStorage.removeItem("name");
    localStorage.removeItem("token");
    this._router.navigate(['login']);
  }

}
