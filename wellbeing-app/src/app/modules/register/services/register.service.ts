import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { RegisterModel } from '../models/register.model';
import { RegisterResultModel } from '../models/register-result.model';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private _requestUrl: string = environment.emotionsUrl + "api/users/register";

  constructor(private _http: HttpClient) {
  }

  async postRegister(registerModel: RegisterModel): Promise<HttpResponse<RegisterResultModel>> {
    return this._http.post<RegisterResultModel>(this._requestUrl, registerModel, { observe: "response"}).toPromise();
  }
}
