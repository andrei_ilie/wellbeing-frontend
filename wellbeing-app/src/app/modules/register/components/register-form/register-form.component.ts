import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { NgModule } from '@angular/core'
import { RegisterService } from '../../services/register.service'
import { RegisterModel} from '../../models/register.model'
import { Router } from '@angular/router'

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})

export class RegisterFormComponent implements OnInit {
  public name: string;
  public email: string;
  public password: string;
  public termsSelected : boolean;
  public rePassword: string;
  public errorMessage: string = "";
  public isValidInput: boolean;

  
  private readonly _passwordTooWeak: string = "Password is too weak.";
  private readonly _passwordsMatch: string = "Passwords don't match.";
  private readonly _notEmail: string = "Not a valid email format";
  private readonly _nullInputs: string = "All fields must be completed";
  private readonly _acceptTerms: string = "You have to accept the terms and conditions";
  private readonly _invalidRequest: string = "Invalid request";
  constructor(private _router: Router, private _registerService: RegisterService) {}

  ngOnInit() {
  }


  private redirectToLogin() {
    this._router.navigate(['login'])
  }

  private changeValidationToInvalid(message: string) {
    this.isValidInput = false;
    this.errorMessage = message;
  }

  register(){
    if(!this.checkInputs()){
      return;
    }
    var registerModel = new RegisterModel();
    registerModel.email = this.email;
    registerModel.name = this.name;
    registerModel.password = this.password;

    this._registerService.postRegister(registerModel)
    .then(response =>{
      if (response.status != 200) {
        this.changeValidationToInvalid(this._invalidRequest);
        return;
      }
      
      this.errorMessage = "Succes!";
      this.redirectToLogin()
    },error=>{
      console.log(error)
    })
  }
  private changeValidationToValid(){
    this.isValidInput = true;
    this.errorMessage = "";
  }

  private checkInputs(){
    var isValid = true;
    if(this.name == "" || this.email == "" || this.password == "" || this.rePassword == ""){
      this.changeValidationToInvalid(this._nullInputs);
      return false;
    }
    if(this.validatePasswords() === false){
      return false;
    } 
    console.log(this.termsSelected)
    if(this.termsSelected == false){
      this.changeValidationToInvalid(this._acceptTerms);
      return false;
    }
    
    if(this.email.indexOf("@") == -1 || this.email.indexOf(".") == -1){
      this.changeValidationToInvalid(this._notEmail);
      return false;
    }
    return true;
  }

  private validatePasswords(): boolean {
    if(this.password == null || this.password.length < 10) {
      this.changeValidationToInvalid(this._passwordTooWeak);
      return false;
    }
    if(this.password != this.rePassword){
      this.changeValidationToInvalid(this._passwordsMatch);
      return false
    }
  }
}
