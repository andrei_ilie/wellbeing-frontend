export class ChartEmotionModel {
    category: string;
    scores: Array<number>;
    subjects: Array<string>;
}