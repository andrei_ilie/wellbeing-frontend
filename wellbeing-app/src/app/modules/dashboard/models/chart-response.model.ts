import { ChartModel } from "./chart.model";

export class ChartResponseModel {
    status: Number;
    metrics: Array<ChartModel>;
}