export class ChartModel {
    day: string;
    emotions: Array<string>;
    values: Array<Number>;
}