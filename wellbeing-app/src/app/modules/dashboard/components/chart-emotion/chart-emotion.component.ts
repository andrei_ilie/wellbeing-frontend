import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { ChartEmotionModel } from '../../models/chart-emotion.model';
import { toArray } from 'rxjs/operators';
//@ts-ignore
import ApexCharts from 'apexcharts';

@Component({
  selector: 'app-chart-emotion',
  templateUrl: './chart-emotion.component.html',
  styleUrls: ['./chart-emotion.component.scss']
})
export class ChartEmotionComponent {

  @Input()
  dates: Subject<ChartEmotionModel>;

  public systemName: string;
  firstCopy = false;

  public lineChartData: Array<number> = [15];
  public lineChartLabels: Array<any> = [13];
 
  public labelMFL: Array<any> = [
  { 
    data: this.lineChartData,
    label: this.systemName
  }];
  
  ngOnInit() {
    this.dates.subscribe(response => {
      console.log(response)
      this.systemName = response.category;
      while(this.lineChartData.length > 0) {
        this.lineChartData.pop();
      }

      response.scores.forEach(x => {
        this.lineChartData.push(x);
      });
      console.log(this.lineChartData);

      while(this.lineChartLabels.length > 0) {
        this.lineChartLabels.pop();
      }

      response.subjects.forEach(x => {
        this.lineChartLabels.push(new Date(parseInt(x)));
      });
      
      while(this.labelMFL.length > 0) {
        this.labelMFL.pop();
      }

      this.labelMFL.push(
        { 
          data: this.lineChartData,
          label: this.lineChartLabels,
          category: response.category
        })
        var options = {
          chart: {
            type: 'bar'
          },
          series: [{
            name: 'sales',
            data: this.lineChartData
          }]
          // xaxis: {
          //   categories: 
          // }
        }
        
        var chart = new ApexCharts(document.querySelector(".charts"), options);
        
        chart.render();
    });
  }

  // public lineChartOptions: any = {
  //   responsive: true,
  //   scales : {
  //     yAxes: [{
  //       ticks: {
  //         max : 50,
  //         min : 0,
  //       }
  //     }],
  //     xAxes: [{
  //       min: '2018-01-29 10:08:00', // how to? 
  //     //  max: '2018-01-29 10:48:00', // how to?
  //       type: 'time',
  //       time: {        
  //         displayFormats: {
  //           'second': 'HH:mm:ss',
  //           'minute': 'HH:mm:ss',
  //           'hour': 'HH:mm',
  //         },
  //       },
  //       }],
  //   },
  // };

  //  _lineChartColors:Array<any> = [{
  //      backgroundColor: 'red',
  //       borderColor: 'red',
  //       pointBackgroundColor: 'red',
  //       pointBorderColor: 'red',
  //       pointHoverBackgroundColor: 'red',
  //       pointHoverBorderColor: 'red' 
  //     }];

  // public lineChartType = 'line';

  public chartClicked(e: any) {
    console.log(this.lineChartData);
    console.log(this.labelMFL);
    console.log(e);
  }

  public chartHovered(e: any) {
    console.log(e);
  }
}
