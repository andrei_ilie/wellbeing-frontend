import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ChartModel } from '../../models/chart.model';
import { DashboardService } from '../../services/dashboard.service';
import { NotificationsService } from 'app/shared/services/notifications.service';
import { ChartEmotionModel } from '../../models/chart-emotion.model';
//@ts-ignore
import ApexCharts from 'apexcharts';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private _dashboardService: DashboardService,
              private _notificationService: NotificationsService) { }

  dates: Subject<Array<ChartModel>> = new Subject();

  date1: Subject<ChartEmotionModel> = new Subject();
  date2: Subject<ChartEmotionModel> = new Subject();
  date3: Subject<ChartEmotionModel> = new Subject();
  date4: Subject<ChartEmotionModel> = new Subject();
  date5: Subject<ChartEmotionModel> = new Subject();
  date6: Subject<ChartEmotionModel> = new Subject();
  date7: Subject<ChartEmotionModel> = new Subject();
  date8: Subject<ChartEmotionModel> = new Subject();

  async ngOnInit() {
    this.updateChart();
    setInterval(this.updateChart.bind(this), 15000);
    setInterval(this.notifyUser.bind(this), 10000);

    this._dashboardService.getEmotionsStatistics().subscribe(response => {
      console.log(response);
      const charts = document.querySelector('.widgets');
      response.forEach((e,i) => {
        var options = {
          chart: {
            type: 'bar'
          },
          series: [{
            name: e.category,
            data: e.scores
          }],
          xaxis: {
            categories: e.subjects.map(dat => new Date(parseInt(dat)).toLocaleDateString())
          }
        }
        
        const newEl = document.createElement('div');
        newEl.classList.add('chart');
        charts.append(newEl);
        const chart = new ApexCharts(document.getElementsByClassName('chart')[i], options);

        chart.render();
      });
      // this.date1.next(response[0]);
      // this.date2.next(response[1]);
      // this.date3.next(response[2]);
      // this.date4.next(response[3]);
      // this.date5.next(response[4]);
      // this.date6.next(response[5]);
      // this.date7.next(response[6]);
      // this.date8.next(response[7]);
    });
  }

  private updateChart() {
    this._dashboardService.getEmotions().subscribe(response => {
        this.dates.next(response.metrics);
    },
    error => {
      console.log(error);
    });
  }

  private async notifyUser() {
    const resp:any = await this._dashboardService.getNotificationMessage();
    this._notificationService.showNotification(resp.title, resp.message);
  }
}
