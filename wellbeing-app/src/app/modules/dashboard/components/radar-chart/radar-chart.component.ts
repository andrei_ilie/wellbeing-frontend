import { Component, Input, OnInit } from '@angular/core';
import { ChartModel } from '../../models/chart.model';
import { Subject } from 'rxjs';
 
@Component({
  selector: 'radar-chart',
  templateUrl: './radar-chart.component.html'
})
export class RadarChartComponent implements OnInit {  
  @Input()
  dates: Subject<Array<ChartModel>>;

  public radarChartType: string = 'radar';
  public demoradarChartLabels: Array<string>;
  public demoradarChartData: Array<any> = [];

  private isInitialize: boolean = false;

  ngOnInit(): void {
    this.dates.pipe().subscribe(chartModels => {
      if(chartModels[0] == null) {
        return;
      }
      
      this.demoradarChartLabels = chartModels[0].emotions;
      while(this.demoradarChartData.length > 0) {
        this.demoradarChartData.pop();
      }
      chartModels.forEach(chartModel => {        
        this.demoradarChartData.push({ data: chartModel.values, label: new Date(parseInt(chartModel.day)).toLocaleDateString() });
      });
      this.isInitialize = true;
    })
  }

  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
}