import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';

import { map, catchError } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { ChartResponseModel } from '../models/chart-response.model';
import { Observable } from 'rxjs';
import { ChartEmotionModel } from '../models/chart-emotion.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private _requestUrl: string = environment.emotionsUrl + "get_metrics";

  constructor(private _http: HttpClient) { 
  }

  getEmotions(): Observable<ChartResponseModel> {
    return this._http.get(this._requestUrl, {headers: new HttpHeaders().set('token', localStorage.getItem('token'))}).pipe(map((response: ChartResponseModel) => response));
  }

  getNotificationMessage() {
    return this._http.get(environment.emotionsUrl + "recommandations", {headers: new HttpHeaders().set("token", localStorage.getItem('token'))}).toPromise();
  }

  getEmotionsStatistics(): Observable<Array<ChartEmotionModel>> {
    return this._http.get(environment.emotionsUrl + "get_category_metrics", {headers: new HttpHeaders().set("token", localStorage.getItem('token'))})
      .pipe(map((response: Array<ChartEmotionModel>) => response));
  }
}
