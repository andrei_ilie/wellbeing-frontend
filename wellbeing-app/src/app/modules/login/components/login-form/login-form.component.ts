import { Component, OnInit, Injectable } from '@angular/core';
import { LoginService } from './../../services/login.service'
import { LoginResultModel } from './../../models/login-result.model';
import { LoginModel } from './../../models/login.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})

@Injectable()
export class LoginFormComponent implements OnInit {
  public email: string;
  public password: string;
  public loginResponse: LoginResultModel;
  public loginError: string = "";

  public isUserInputValid: boolean = true;

  private readonly _errorMessage: string = "The email or the password is not valid.";
  private readonly _invalidRequestErrorMessage: string = "The combination email - password is not valid.";

  constructor(private _router: Router, private _loginService: LoginService) {}

  ngOnInit() {
  }

  async login() {
    if(!this.isTheInputValid()) {
      return;
    }

    var loginModel = new LoginModel();
    loginModel.email = this.email;
    loginModel.password = this.password;

    this._loginService.postLogin(loginModel)
    .then(response => {
      if (response.status != 200) {
        this.changeValidationToInvalid(this._invalidRequestErrorMessage);
        return;
      }

      localStorage.setItem("name", response.body.name);
      localStorage.setItem("token", response.body.token);
      this.redirectToDashboard();
    },
    error => {
      console.log(error);
    });
  }

  private redirectToDashboard() {
    this._router.navigate(['dashboard'])
  }

  private redirectToRegister() {
    this._router.navigate(['register'])
  }

  private changeValidationToValid() {
    this.isUserInputValid = true;
    this.loginError = "";
  }

  private changeValidationToInvalid(message: string) {
    this.isUserInputValid = false;
    this.loginError = message;
  }

  private isTheInputValid(): boolean {
    if(!this.validateEmail() || !this.validatePassword()) {
      this.changeValidationToInvalid(this._errorMessage);
      return false;
    }

    this.changeValidationToValid();
    return true;
  }

  private validateEmail(): boolean {
    if(this.email == null) {
      return false;
    }

    const emailRegex = new RegExp('[a-z|A-Z|0-9|.|-|_]+@[a-z|A-Z|.]+');
    return emailRegex.test(this.email)
  }

  private validatePassword(): boolean {
    if(this.password == null || this.password.length < 5) {
      this.changeValidationToInvalid(this._errorMessage);
      return false;
    }

    return true;
  }
}