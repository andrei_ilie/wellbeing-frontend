import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { LoginModel } from '../models/login.model';
import { LoginResultModel } from '../models/login-result.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // private _requestUrl: string = environment.emotionsUrl + "api/users/login";
  private _requestUrl: string = "http://localhost:3000/api/users/" + "login";
  constructor(private _http: HttpClient) { 
  }

  postLogin(loginModel: LoginModel): Promise<HttpResponse<LoginResultModel>> {
    return this._http.post<LoginResultModel>(this._requestUrl, loginModel, { observe: "response"}).toPromise();
  }
}
