export class LoginResultModel {
    token: string;
    name: string;
    email: string;
}